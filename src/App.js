import React from 'react';
import logo from './logo.svg';
import './App.css';
import Greet from './components/Greet/Greet';
import Header from './components/Header/Header';

function App() {
  return (
    <div className="App">
      <Header />
      
      <Greet name="Sara" />
    </div>
  );
}

export default App;
